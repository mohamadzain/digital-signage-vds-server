"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.db_update = exports.db = void 0;

var _lowdb = _interopRequireDefault(require("lowdb"));

var _mkdirp = _interopRequireDefault(require("mkdirp"));

var _FileSync = _interopRequireDefault(require("lowdb/adapters/FileSync"));

var _config = _interopRequireDefault(require("../config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const db = (0, _lowdb.default)(new _FileSync.default('db.json')); // // Seed an empty DB

exports.db = db;
db.defaults({
  uploads: []
}).write(); // Ensure upload directory exists

_mkdirp.default.sync(_config.default.uploadsDir);

const db_update = file => db.get('uploads').push(file).last().write();

exports.db_update = db_update;