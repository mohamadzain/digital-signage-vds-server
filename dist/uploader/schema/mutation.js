"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.mutationResolvers = exports.mutationTypes = void 0;

var _promisesAll = _interopRequireDefault(require("promises-all"));

var file_model = _interopRequireWildcard(require("../../models/file"));

var file_service = _interopRequireWildcard(require("../../service/file"));

var _subscription = require("./subscription");

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const mutationTypes = [`
  extend type Mutation {
    singleUpload(file: Upload!): File!
    multipleUpload(files: [Upload!]!): [File!]!
  }
`];
exports.mutationTypes = mutationTypes;
const mutationResolvers = {
  Mutation: {
    async singleUpload(obj, {
      file
    }) {
      const {
        createReadStream,
        filename,
        mimetype
      } = await file;
      const stream = createReadStream();
      const {
        id,
        path
      } = await file_service.store({
        stream,
        filename
      });

      _subscription.pubsub.publish('fileAdded', {
        fileAdded: {
          id,
          filename,
          mimetype,
          path
        }
      });

      return file_model.db_update({
        id,
        filename,
        mimetype,
        path
      });
    },

    async multipleUpload(obj, {
      files
    }) {
      const {
        resolve,
        reject
      } = await _promisesAll.default.all(files.map(processUpload));
      if (reject.length) reject.forEach(({
        name,
        message
      }) => // eslint-disable-next-line no-console
      console.error(`${name}: ${message}`));
      return resolve;
    }

  }
};
exports.mutationResolvers = mutationResolvers;