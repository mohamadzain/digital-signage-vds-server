"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.fileTypeResolvers = exports.fileType = void 0;
const fileType = [`
type File {
    id: ID!
    path: String!
    filename: String!
    mimetype: String!
  }
`];
exports.fileType = fileType;
const fileTypeResolvers = {};
exports.fileTypeResolvers = fileTypeResolvers;