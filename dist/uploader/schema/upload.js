"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.typeResolvers = exports.uploadTypes = void 0;

var _graphqlUpload = require("graphql-upload");

const uploadTypes = [`
scalar Upload
`];
exports.uploadTypes = uploadTypes;
const typeResolvers = {
  Upload: _graphqlUpload.GraphQLUpload
};
exports.typeResolvers = typeResolvers;