"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.subscriptionResolvers = exports.subscriptionTypes = exports.pubsub = void 0;

var _graphqlSubscriptions = require("graphql-subscriptions");

const pubsub = new _graphqlSubscriptions.PubSub();
exports.pubsub = pubsub;
const subscriptionTypes = [`
extend type Subscription {
  fileAdded: Upload
}`];
exports.subscriptionTypes = subscriptionTypes;
const subscriptionResolvers = {
  Subscription: {
    fileAdded: {
      resolve: payload => {
        return payload;
      },
      subscribe: () => pubsub.asyncIterator('fileAdded')
    }
  }
};
exports.subscriptionResolvers = subscriptionResolvers;