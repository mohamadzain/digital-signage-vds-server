"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.queryResolvers = exports.queryTypes = void 0;

var file_model = _interopRequireWildcard(require("../../models/file"));

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) { var desc = Object.defineProperty && Object.getOwnPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : {}; if (desc.get || desc.set) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } } newObj.default = obj; return newObj; } }

const queryTypes = [`
extend type Query {
  uploads: [File]
}
`];
exports.queryTypes = queryTypes;
const queryResolvers = {
  Query: {
    uploads: () => file_model.db.get('uploads').value()
  }
};
exports.queryResolvers = queryResolvers;