"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.resolvers = exports.schema = void 0;

var _file = require("./schema/file");

var _query = require("./schema/query");

var _upload = require("./schema/upload");

var _mutation = require("./schema/mutation");

var _subscription = require("./schema/subscription");

const schema = [..._file.fileType, ..._query.queryTypes, ..._upload.uploadTypes, ..._mutation.mutationTypes, ..._subscription.subscriptionTypes];
exports.schema = schema;
const resolvers = Object.assign(_query.queryResolvers, _mutation.mutationResolvers, _subscription.subscriptionResolvers, _file.fileTypeResolvers);
exports.resolvers = resolvers;