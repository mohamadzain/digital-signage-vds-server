"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;
const commonConfig = {
  env: process.env.NODE_ENV || 'development',
  port: parseInt(process.env.PORT, 10) || 5000,
  corsDomain: process.env.CORS_DOMAIN || '*',
  uploadsDir: './uploads'
};
var _default = commonConfig;
exports.default = _default;