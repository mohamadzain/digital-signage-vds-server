"use strict";

var _file = require("../models/file");

var _path = _interopRequireDefault(require("path"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.file_get = (req, res) => {
  const fileId = req.params.fileId;

  const fileInfo = _file.db.get('uploads').find({
    id: fileId
  }).value();

  console.log(fileInfo);

  if (fileInfo.id) {
    res.download(_path.default.resolve(__dirname, '../../', fileInfo.path), err => {
      console.log(err);
    });
  } else {
    res.send(404);
  }
};