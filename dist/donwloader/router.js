"use strict";

var _express = require("express");

var _controller = _interopRequireDefault(require("./controller"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const routes = (0, _express.Router)();
routes.get('/api/video/:fileId', _controller.default.file_get);
module.exports = routes;