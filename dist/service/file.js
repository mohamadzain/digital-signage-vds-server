"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.store = void 0;

var _fs = _interopRequireDefault(require("fs"));

var _shortid = _interopRequireDefault(require("shortid"));

var _config = _interopRequireDefault(require("../config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const store = ({
  stream,
  filename
}) => {
  const id = _shortid.default.generate();

  const path = `${_config.default.uploadsDir}/${id}-${filename}`;
  return new Promise((resolve, reject) => stream.on('error', error => {
    if (stream.truncated) // Delete the truncated file
      _fs.default.unlinkSync(path);
    reject(error);
  }).pipe(_fs.default.createWriteStream(path)).on('error', error => reject(error)).on('finish', () => resolve({
    id,
    path
  })));
};

exports.store = store;