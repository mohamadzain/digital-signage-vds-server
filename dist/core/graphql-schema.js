"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.executableSchema = void 0;

var _lodash = require("lodash");

var _graphqlTools = require("graphql-tools");

var _uploader = require("../uploader");

const rootResolvers = {
  Query: {
    status: () => 'ok'
  }
};
const rootSchema = [`
      type Query {
        status: String
      }
    
      type Mutation {
        _empty: String
      }
    
      type Subscription {
        _empty: String
      }

      schema {
        query: Query,
        mutation : Mutation,
        subscription : Subscription
      }
    `];
const schema = [...rootSchema, ..._uploader.schema];
const resolvers = (0, _lodash.merge)(rootResolvers, _uploader.resolvers);
const executableSchema = (0, _graphqlTools.makeExecutableSchema)({
  typeDefs: schema,
  resolvers
});
exports.executableSchema = executableSchema;