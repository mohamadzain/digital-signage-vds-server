"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.graphqlSubscriptionServer = exports.graphqlInitialise = void 0;

var _bodyParser = _interopRequireDefault(require("body-parser"));

var _graphql = require("graphql");

var _apolloServerExpress = require("apollo-server-express");

var _graphqlUpload = require("graphql-upload");

var _subscriptionsTransportWs = require("subscriptions-transport-ws");

var _config = _interopRequireDefault(require("../config"));

var _graphqlSchema = require("./graphql-schema");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const graphqlInitialise = app => {
  app.use((0, _graphqlUpload.graphqlUploadExpress)({
    maxFileSize: Infinity,
    // 10 MB
    maxFiles: 5
  }));

  if (_config.default.env === 'development') {
    const port = app.get('port') || _config.default.port;

    app.use('/api/graphiql', (0, _apolloServerExpress.graphiqlExpress)({
      endpointURL: '/api',
      subscriptionsEndpoint: `ws://localhost:${port}/subscriptions`
    }));
  }

  app.use('/api', _bodyParser.default.json(), (0, _apolloServerExpress.graphqlExpress)(() => ({
    schema: _graphqlSchema.executableSchema,
    debug: _config.default.env === 'development'
  })));
};

exports.graphqlInitialise = graphqlInitialise;

const graphqlSubscriptionServer = ws => {
  new _subscriptionsTransportWs.SubscriptionServer({
    execute: _graphql.execute,
    subscribe: _graphql.subscribe,
    schema: _graphqlSchema.executableSchema
  }, {
    server: ws,
    path: '/subscriptions'
  });
};

exports.graphqlSubscriptionServer = graphqlSubscriptionServer;