"use strict";

var _express = require("express");

var _router = _interopRequireDefault(require("../donwloader/router"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const router = (0, _express.Router)();
router.use(_router.default);
module.exports = router;