"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = void 0;

var _express = _interopRequireDefault(require("express"));

var _http = require("http");

var _cors = _interopRequireDefault(require("cors"));

var _routes = _interopRequireDefault(require("./routes"));

var _graphql = require("./graphql");

var _config = _interopRequireDefault(require("../config"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// HTTP SERVER
// GraphQL - Apollo
// Config
const app = (0, _express.default)();

function setPort(port = 5000) {
  app.set('port', parseInt(port, 10));
}

function listen() {
  const port = app.get('port') || _config.default.port;

  const ws = (0, _http.createServer)(app);
  ws.listen(port, () => {
    (0, _graphql.graphqlSubscriptionServer)(ws);
    console.log(`The server is running and listening at http://localhost:${port}`);
  });
}

app.use((0, _cors.default)({
  origin: _config.default.corsDomain,
  // Be sure to switch to your production domain
  optionsSuccessStatus: 200
})); // Endpoint to check if the API is running

app.get('/api/status', (req, res) => {
  res.send({
    status: 'ok'
  });
});
app.use(_routes.default); // Append apollo to our API

(0, _graphql.graphqlInitialise)(app);
var _default = {
  getApp: () => app,
  setPort,
  listen
};
exports.default = _default;