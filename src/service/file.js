import fs from 'fs';
import shortid from 'shortid';
import config from '../config';

export const store = ({ stream, filename }) => {
    const id = shortid.generate();
    const path = `${config.uploadsDir}/${id}-${filename}`;
    return new Promise((resolve, reject) =>
      stream
        .on('error', error => {
          if (stream.truncated)
            // Delete the truncated file
            fs.unlinkSync(path);
          reject(error);
        })
        .pipe(fs.createWriteStream(path))
        .on('error', error => reject(error))
        .on('finish', () => resolve({ id, path }))
    );
  };