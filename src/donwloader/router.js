import { Router } from 'express';
import video_controller from './controller';

const routes = Router();
routes.get('/api/video/:fileId', video_controller.file_get);

module.exports = routes;
