import { db } from '../models/file';
import path from 'path';

exports.file_get = (req, res) => {
  const fileId = req.params.fileId;
  const fileInfo = db
    .get('uploads')
    .find({ id: fileId })
    .value();
  console.log(fileInfo);
  if (fileInfo.id) {
    res.download(path.resolve(__dirname, '../../', fileInfo.path), err => {
      console.log(err);
    });
  } else {
    res.send(404);
  }
};
