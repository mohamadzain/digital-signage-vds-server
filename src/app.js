import server from './core/server';

server.listen();

export default server;
