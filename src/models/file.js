import lowdb from 'lowdb';
import mkdirp from 'mkdirp';
import FileSync from 'lowdb/adapters/FileSync';
import config from '../config';

export const db = lowdb(new FileSync('db.json'));
// // Seed an empty DB
db.defaults({ uploads: [] }).write();

// Ensure upload directory exists
mkdirp.sync(config.uploadsDir);

export const db_update = file =>
  db
    .get('uploads')
    .push(file)
    .last()
    .write();
