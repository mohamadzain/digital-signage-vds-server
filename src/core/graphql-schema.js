import { merge } from 'lodash';
import { makeExecutableSchema } from 'graphql-tools';
import {
  schema as fileUploadSchema,
  resolvers as fileUploadResolvers
} from '../uploader';

const rootResolvers = {
  Query: {
    status: () => 'ok'
  }
};

const rootSchema = [
  `
      type Query {
        status: String
      }
    
      type Mutation {
        _empty: String
      }
    
      type Subscription {
        _empty: String
      }

      schema {
        query: Query,
        mutation : Mutation,
        subscription : Subscription
      }
    `
];

const schema = [...rootSchema, ...fileUploadSchema];
const resolvers = merge(rootResolvers, fileUploadResolvers);

export const executableSchema = makeExecutableSchema({
  typeDefs: schema,
  resolvers
});
