// HTTP SERVER
import express from 'express';
import { createServer } from 'http';
import cors from 'cors';
import routes from './routes';

// GraphQL - Apollo
import { graphqlInitialise, graphqlSubscriptionServer } from './graphql';

// Config
import config from '../config';

const app = express();

function setPort(port = 5000) {
  app.set('port', parseInt(port, 10));
}

function listen() {
  const port = app.get('port') || config.port;
  const ws = createServer(app);
  ws.listen(port, () => {
    graphqlSubscriptionServer(ws);

    console.log(
      `The server is running and listening at http://localhost:${port}`
    );
  });
}

app.use(
  cors({
    origin: config.corsDomain, // Be sure to switch to your production domain
    optionsSuccessStatus: 200
  })
);

// Endpoint to check if the API is running
app.get('/api/status', (req, res) => {
  res.send({ status: 'ok' });
});

app.use(routes);

// Append apollo to our API
graphqlInitialise(app);

export default {
  getApp: () => app,
  setPort,
  listen
};
