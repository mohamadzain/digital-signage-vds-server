import bodyParser from 'body-parser';
import { execute, subscribe } from 'graphql';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { graphqlUploadExpress } from 'graphql-upload';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import config from '../config';
import { executableSchema } from './graphql-schema';

export const graphqlInitialise = app => {
  app.use(
    graphqlUploadExpress({
      maxFileSize: Infinity, // 10 MB
      maxFiles: 5
    })
  );

  if (config.env === 'development') {
    const port = app.get('port') || config.port;
    app.use(
      '/api/graphiql',
      graphiqlExpress({
        endpointURL: '/api',
        subscriptionsEndpoint: `ws://localhost:${port}/subscriptions`
      })
    );
  }

  app.use(
    '/api',
    bodyParser.json(),
    graphqlExpress(() => ({
      schema: executableSchema,
      debug: config.env === 'development'
    }))
  );
};

export const graphqlSubscriptionServer = ws => {
  new SubscriptionServer(
    {
      execute,
      subscribe,
      schema: executableSchema
    },
    {
      server: ws,
      path: '/subscriptions'
    }
  );
};
