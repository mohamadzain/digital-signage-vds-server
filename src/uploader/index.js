import { fileType, fileTypeResolvers } from './schema/file';
import { queryTypes, queryResolvers } from './schema/query';
import { uploadTypes } from './schema/upload';
import { mutationTypes, mutationResolvers } from './schema/mutation';
import { subscriptionTypes, subscriptionResolvers } from './schema/subscription';


export const schema = [
  ...fileType,
  ...queryTypes,
  ...uploadTypes,
  ...mutationTypes,
  ...subscriptionTypes
];

export const resolvers = Object.assign(
  queryResolvers,
  mutationResolvers,
  subscriptionResolvers,
  fileTypeResolvers
);
