import * as file_model from '../../models/file';

export const queryTypes = [
  `
extend type Query {
  uploads: [File]
}
`
];

export const queryResolvers = {
  Query: {
    uploads: () => file_model.db.get('uploads').value()
  }
};
