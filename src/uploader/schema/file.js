export const fileType = [`
type File {
    id: ID!
    path: String!
    filename: String!
    mimetype: String!
  }
`];

export const fileTypeResolvers = {
};



