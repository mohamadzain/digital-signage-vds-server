import { PubSub } from 'graphql-subscriptions';

export const pubsub = new PubSub();

export const subscriptionTypes = [
  `
extend type Subscription {
  fileAdded: Upload
}`
];

export const subscriptionResolvers = {
  Subscription: {
    fileAdded: {
      resolve: payload => {
        return payload;
      },
      subscribe: () => pubsub.asyncIterator('fileAdded')
    }
  }
};
