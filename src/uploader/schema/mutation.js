import promisesAll from 'promises-all';

import * as file_model from '../../models/file';
import * as file_service from '../../service/file'

import { pubsub } from './subscription';

export const mutationTypes = [
  `
  extend type Mutation {
    singleUpload(file: Upload!): File!
    multipleUpload(files: [Upload!]!): [File!]!
  }
`
];

export const mutationResolvers = {
  Mutation: {
    async singleUpload(obj, { file }) {
      const { createReadStream, filename, mimetype } = await file;
      const stream = createReadStream();
      const { id, path } = await file_service.store({ stream, filename });

      pubsub.publish('fileAdded', {
        fileAdded: { id, filename, mimetype, path }
      });

      return file_model.db_update({ id, filename, mimetype, path });
    },
    async multipleUpload(obj, { files }) {
      const { resolve, reject } = await promisesAll.all(
        files.map(processUpload)
      );

      if (reject.length)
        reject.forEach(({ name, message }) =>
          // eslint-disable-next-line no-console
          console.error(`${name}: ${message}`)
        );

      return resolve;
    }
  }
};
