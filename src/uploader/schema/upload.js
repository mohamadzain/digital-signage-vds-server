import { GraphQLUpload } from 'graphql-upload';

export const uploadTypes =[ `
scalar Upload
`];


export const typeResolvers = {
    Upload: GraphQLUpload,
};


